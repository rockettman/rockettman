<!--
**rockettman/rockettman** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your GitLab profile.
-->
### Hi, welcome to my personal projects!
It must be said... I do like things besides Harry Potter.

#### [FolioMagi](https://gitlab.com/rockettman/foliomagi) - A web app for building Harry Potter TCG decks
Please contribute if you're interested!

#### [Weasley Clock](https://gitlab.com/rockettman/weasleyclock) - Non-creepy location tracking for the whole family to increase sense of closeness when you live far apart
Stalled when I started my most recent job. Will pick back up in 2024 in preparation for moving abroad.

#### [launchpad](https://gitlab.com/rockettman/launchpad) - My automated dev env setup for 🐧 and 🍎 but almost definitely not 🪟

---

I also use GitHub when necessary: https://github.com/arockett

Fantastic profile pic courtesy of [dedacab](https://dedacab.tumblr.com/)
